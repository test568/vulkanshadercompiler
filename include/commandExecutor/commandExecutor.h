#ifndef COMMAND_EXECUTOR_H
#define COMMAND_EXECUTOR_H

#include "commandReader/command.h"

class CommandExecutor
{
private:
public:
    CommandExecutor() {}
    ~CommandExecutor() {}

    void executeCommand(Command *_command, std::ostream &_errorOutputs);
};

#endif
#ifndef COMMAND_DESCRIPTION_H
#define COMMAND_DESCRIPTION_H

#include <string>
#include <list>
#include <vector>
#include <map>

enum TypeDataSetting : int
{
    TypeDataSetting_Unsplit = 0,
    TypeDataSetting_Split = 1,
    TypeDataSetting_Toogle = 2,
};

enum TypeSetting : int
{
    TypeSetting_Strng = 0,
    TypeSetting_Number = 1,
    TypeSetting_Integer = 2,
};

class CommandDescription
{
public:
    struct CommandSetting
    {
        int countDatas;
        TypeDataSetting typeGettingSetting;
        TypeSetting typeSetting;

        std::string nameSetting;
    };
    
    struct SettingDatas
    {   
        std::vector<std::string> stringsDatas;
        std::vector<int> integersDatas;
        std::vector<float> numbersDatas;
        bool enable;
    };

private:
    std::string commandName;

    std::vector<CommandSetting> settings;

public:
    void addCommandSettings(int _countDatas, TypeDataSetting _typeData, TypeSetting _type, const char *_name);

    int getCountSettings() const { return settings.size(); };

    const CommandSetting *getCommandSetting(int _index) const { return &(settings[_index]); }

    void setCommandName(const char *_name) { commandName = std::string(_name); }

    const char *getCommandName() const { return commandName.c_str(); }
};

#endif
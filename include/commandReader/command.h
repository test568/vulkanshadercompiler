#ifndef COMMAND_H
#define COMMAND_H

#include <map>
#include <string>
#include <ostream>

#include "commandDescription.h"

class Command;

typedef Command* (*CreateCommand)(const std::map<std::string, CommandDescription::SettingDatas> &_settings, std::ostream &_errorOutput);

class Command
{
protected:

public:
    Command() {}

    virtual ~Command() {}

    virtual void execute(std::ostream &_errorOutput) = 0;
};

#endif
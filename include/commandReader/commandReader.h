#ifndef COMMAND_READER_H
#define COMMAND_READER_H

#include <ostream>
#include <vector>

#include "command.h"
#include "commandDescription.h"

class CommandReader
{
private:
    std::vector<std::pair<CommandDescription, CreateCommand>> commandDescriptions;

public:
    void addCommandType(CommandDescription _description, CreateCommand _createFunctions);

    bool readCommands(int _argc, char *_argv[], std::vector<Command *> *_outBufferCommands, std::ostream &_errorOutput);

private:
    void addSettingData(const CommandDescription::CommandSetting *_setting, const std::string &_data, std::map<std::string, CommandDescription::SettingDatas> &_outSetSettings, std::ostream &_errorOutput);

    void setEnableSetting(const CommandDescription::CommandSetting *_setting, std::map<std::string, CommandDescription::SettingDatas> &_outSetSettings);
};


#endif
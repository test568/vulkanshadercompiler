#ifndef FILE_H
#define FILE_H

#include <string>
#include <vector>

struct File
{
    std::string name;
    std::string extension;
    std::string root;

    std::string content;
};

struct BinaryFile
{
    std::string name;
    std::string extension;
    std::string root;

    std::vector<char> content;
};

#endif
#ifndef PATH_UTILS_H
#define PATH_UTILS_H

#include <string>

class PathUtils
{
public:
    static std::string getRootDirectoryFile(const std::string &_pathToFile);

    static std::string combinePaths(const std::string &_firstPath, const std::string &_secondPath);

    static std::string correctFolderPath(const std::string &_pathToFolder);
};

#endif
#ifndef UTILS_RW_H
#define UTILS_RW_H

#include <string>
#include <vector>
#include <iostream>

class UtilsRW
{
public:
    void writeInt(std::ostream &_file, int _value) const;

    int readInt(std::istream &_file) const;

    void writeFloat(std::ostream &_file, float _value) const;

    float readFloat(std::istream &_file) const;

    void writeString(std::ostream &_file, const std::string &_str) const;

    std::string readString(std::istream &_file) const;

    void writeBool(std::ostream &_file, bool &_value) const;

    bool readBool(std::istream &_file) const;

    void writeBufferUInt(std::ostream &_file, const std::vector<unsigned int> &_buf) const;

    bool readBufferUInt(std::istream &_file, std::vector<unsigned int> &_buf) const;
};

#endif
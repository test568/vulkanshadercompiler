#ifndef FILE_RW_H
#define FILE_RW_H

#include <ostream>

#include "file.h"

class FileRW
{
public:
    File *readFile(const char *_path) const;

    bool writeFile(const File *_file) const;

    BinaryFile *readBinaryFile(const char *_path) const;

    bool writeBinaryFile(const BinaryFile *_file) const;
};

#endif
#ifndef PACKET_SHADERS_H
#define PACKET_SHADERS_H

#include <vector>

#include "shaderData.h"

class PacketShaders
{
private:
    std::vector<ShaderData *> shadersDatas;

public:
    PacketShaders();

    ~PacketShaders();

    void addShader(const ShaderData &_shaderData);

    void addPacketShaders(const PacketShaders *_packetShaders);

    int countShaders() const;

    const ShaderData *getShader(int _index) const;
};

#endif
#ifndef SHADER_DATA_H
#define SHADER_DATA_H

#include <string>
#include <vector>

enum ShaderType : int
{
    ShaderType_Vertex = 0,
    ShaderType_Fragment = 1,
    ShaderType_Compute = 2,
};

enum DataType : int
{
    DataType_Float,
    DataType_Double,
    DataType_Float16,
    DataType_Int8,
    DataType_Uint8,
    DataType_Int16,
    DataType_Uint16,
    DataType_Int,
    DataType_Uint,
    DataType_Int64,
    DataType_Uint64,
    DataType_Bool,
};

enum SamplerFormat : int
{
    SamplerFormat_1D,
    SamplerFormat_2D,
    SamplerFormat_3D,
    SamplerFormat_Cube,
    SamplerFormat_Rect,
    SamplerFormat_Buffer,
};

enum BlockStorageType : int
{
    BlockStorage_UniformBuffer = 0,
    BlockStorage_StorageBuffer = 1,
    BlockStorage_PushConstant = 2,
};

struct UniformVariable
{
    std::string name;
    DataType type;
    int offset;
    int size;
    int vectorSize;
    int implicitArraySize;
};

struct UniformBlock
{
    std::string name;
    int size;
    int set;
    int binding;
    BlockStorageType blockType;
    bool isPushConstant;
    std::vector<UniformVariable> variables;
};

struct UniformSampler
{
    std::string name;
    int set;
    int binding;
    SamplerFormat samplerFormat;
    DataType dataType;
    int size;
    int vectorSize;
    int implicitArraySize;

    bool isImage;
    bool isShadow;
    bool isArray;
};

struct ShaderData
{
    std::string nameShader;
    ShaderType type;
    std::vector<unsigned int> spirvCode;
    std::vector<UniformBlock> uniformBlocks;
    std::vector<UniformSampler> uniformSamplers;
};

#endif
#ifndef SHADER_H
#define SHADER_H

#include <glslang/Public/ShaderLang.h>
#include "shaderData.h"


class Shader
{
private:
    std::string name;
    int version;
    EShLanguage type;
    glslang::TShader *tshader;
    glslang::TProgram *tprogram;

public:
    Shader(const char *_name, int _version, EShLanguage _type, glslang::TShader *_tshader, glslang::TProgram *_tprogram);

    ~Shader();

    const char *getName() const { return name.c_str(); }

    EShLanguage getType() const { return type; }

    bool getShaderData(ShaderData *_data);

};

#endif
#ifndef SHADER_COMPILER_H
#define SHADER_COMPILER_H

#include <glslang/Public/ShaderLang.h>
#include <ostream>
#include <string>
#include <vector>

#include "shader.h"
#include "shaderConfig.h"

class ShaderCompiler
{
public:
    class IncludeProvider : public glslang::TShader::Includer
    {
    private:
        std::string rootShaderFile;
        std::vector<std::string> librariesFolders;
        std::vector<IncludeResult *> createdIncludes;

    public:
        IncludeProvider(const std::string &_rootShaderFile);

        virtual ~IncludeProvider();

        void addLibraryFolder(const char *_path) { librariesFolders.push_back(std::string(_path)); }

        virtual IncludeResult *includeSystem(const char *_headerName, const char *_includerName, size_t _inclusionDepth) override;

        virtual IncludeResult *includeLocal(const char *_headerName, const char *_includerName, size_t _inclusionDepth) override;

        virtual void releaseInclude(IncludeResult *_include) override;

    };

public:
    Shader *compilerShader(const ShaderConfig *_config, int _indexVariant, std::ostream &_errorOutput);

private:
    TBuiltInResource getDefaultResources();
};

#endif
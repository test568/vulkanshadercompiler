#ifndef SHADER_CONFIG_H
#define SHADER_CONFIG_H

#include <string>
#include <ostream>
#include <glslang/Public/ShaderLang.h>

class ShaderConfig
{
public:
    struct Variant
    {
        std::string suffix;
        std::vector<std::string> keywords;  
    };
    
private:
    int version;
    EShLanguage typeShader;
    glslang::EShTargetClientVersion vulkanTargetVersion;
    glslang::EShTargetLanguageVersion spirvTargetVersion;
    std::string inputFilePath;
    std::vector<std::string> librariesFolders;
    std::string rootPathShader;
    std::string nameShader;
    std::vector<Variant> variants;

    ShaderConfig() {}

    void setVersion(int _version) { version = _version; }

    void setTypeShader(EShLanguage _typeShader) { typeShader = _typeShader; }

    void setVulkanTargetVersion(glslang::EShTargetClientVersion _vulkanTargetVersion) { vulkanTargetVersion = _vulkanTargetVersion; }

    void setSPIRVTargetVersion(glslang::EShTargetLanguageVersion _spirvTargetVersion) { spirvTargetVersion = _spirvTargetVersion; }

    void setInputFilePath(const char *_path) { inputFilePath = std::string(_path); }

    void addLibraryFolder(const char *_path) { librariesFolders.push_back(std::string(_path)); }

    void setRootPathShader(const std::string &_rootPathShader) { rootPathShader = _rootPathShader; }

    void setNameShader(const char *_name) { nameShader = std::string(_name); }

    void addVariant(const Variant *_variant) { variants.push_back(*_variant); }

public:
    ~ShaderConfig() {}

    static ShaderConfig *readShaderConfig(const char *_path, std::ostream &_errorOutput);

    int getVersion() const { return version; }

    EShLanguage getTypeShader() const { return typeShader; }

    glslang::EShTargetClientVersion getVulkanTargetVersion() const { return vulkanTargetVersion; }

    glslang::EShTargetLanguageVersion getSPIRVTargetVersion() const { return spirvTargetVersion; }

    const char *getInputFilePath() const { return inputFilePath.data(); }

    int getCountLibrariesFolders() const { return librariesFolders.size(); }

    const char *getPathToLibraryFolder(int _index) const { return librariesFolders[_index].data(); }

    const std::string &getRootPathShader() const { return rootPathShader; }

    const char *getNameShader() const { return nameShader.data(); }

    int getCountVariants() const { return variants.size(); }

    const Variant *getVariant(int _index) const  { return variants.data() + _index; }
};

#endif
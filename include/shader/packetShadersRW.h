#ifndef PACKET_SHADERS_RW_H
#define PACKET_SHADERS_RW_H

#include <iostream>

#include "utils/utilsRW.h"
#include "packetShaders.h"

class PacketShadersRW : protected UtilsRW
{
private:
    void readUniformSamplers(ShaderData *_shaderDatas, std::istream &_in) const;

    void writeUniformSamplers(const ShaderData *_shaderDatas, std::ostream &_out) const;

    void readUniformBlocks(ShaderData *_shaderDatas, std::istream &_in) const;

    void writeUniformBlocks(const ShaderData *_shaderDatas, std::ostream &_out) const;

public:
    void read(PacketShaders *_packetShaders, std::istream &_in) const;

    void write(const PacketShaders *_packetShaders, std::ostream &_out) const;

    bool readFromFile(const char *_path, PacketShaders *_packetShaders) const;

    bool writeToFile(const char *_path, const PacketShaders *_packetShaders) const;
};

#endif
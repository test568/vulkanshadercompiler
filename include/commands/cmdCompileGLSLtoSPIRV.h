#ifndef CMD_COMPILE_GLSL_TO_SPIRV_H
#define CMD_COMPILE_GLSL_TO_SPIRV_H

#include <ostream>
#include <vector>
#include <string>

#include "commandReader/command.h"
#include "commandReader/commandDescription.h"

#include "utils/file.h"
#include "shader/shader.h"
#include "shader/shaderConfig.h"
#include "shader/packetShaders.h"

class CmdCompileGLSLtoSPIRV : Command
{
private:
    std::vector<std::string> inputFiles;
    std::string outputFolder;
    bool seprateOutput;
    std::string nameOutputFile;

    std::vector<ShaderConfig *> readShaedrsConfigs;
    std::vector<PacketShaders *> packetsShaders;
    std::vector<std::string> namesPackets;

    CmdCompileGLSLtoSPIRV(std::vector<std::string> &_inputFiles, std::string &_outputFolder,
                          bool &_seprateOutput, std::string &_nameOutputFile);

public:
    virtual ~CmdCompileGLSLtoSPIRV();

    static Command *createCommand(const std::map<std::string, CommandDescription::SettingDatas> &_settings, std::ostream &_errorOutput);

    static CommandDescription getCommandDescription();

    void execute(std::ostream &_errorOutput) override;

    Shader *compileShader(ShaderConfig *_shaderConfig, int _indexVariant, std::ostream &_errorOutput);

    void writeWarningsInShader(ShaderData *_data, std::ostream &_errorOutput);

    void writeShadersPacket(PacketShaders *_packet, const std::string &_name, std::ostream &_errorOutput);
};

#endif
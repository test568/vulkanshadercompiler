#ifndef CMD_MERGE_PACKAGES_H
#define CMD_MERGE_PACKAGES_H

#include <ostream>
#include <vector>
#include <string>

#include "commandReader/command.h"
#include "commandReader/commandDescription.h"

#include "utils/file.h"
#include "utils/fileRW.h"
#include "shader/shader.h"
#include "shader/shaderConfig.h"
#include "shader/packetShaders.h"

class CmdMergePackages : Command
{
private:
    std::vector<std::string> inputFiles;
    std::string outputFolder;
    std::string nameOutputFile;

    std::vector<PacketShaders *> packetsLoaded;

    CmdMergePackages(std::vector<std::string> &_inputFiles, std::string &_outputFolder, std::string &_nameOutputFile);

public:
    virtual ~CmdMergePackages();

    static Command *createCommand(const std::map<std::string, CommandDescription::SettingDatas> &_settings, std::ostream &_errorOutput);

    static CommandDescription getCommandDescription();

    void execute(std::ostream &_errorOutput) override;
};

#endif
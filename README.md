
# Vulkan Shader Compiler

This project was created for [Physics](https://gitlab.com/Weaponer/physics.git) to compilation glsl shader to SPIR-V code with metadata. 




## Dependencies
- Vulkan API

## Usage/Examples

Command settings:
- i - input files.
- o - output files.
- n - name output file.
- separate - separate output files.
- compile - compile input shaders to packages.
- merge - merge input packages to output package.

An example of how to compile input shaders in one package or separate packages:

```bash
  ./output/sprvCompiler compile -i shaders/basicRender.cfg -nrender -ocshaders
```
An example of how to merge packages:

```bash
  ./output/sprvCompiler merge -i packageOne packageTwo -nrender -opackages
```

Example shader description (cfg file):

```cfg
name = "basicRenderFrag"
version = 450
vulkanVersion = 1.0
spirvVersion = 1.0
type = "fragment"
file = "shaders/basicRender/frag.frag"
libraries = [ "shaders/libs" ]
variants = [ 
        {
            suffix = "_0"
            keywords = [ "TEST" ]
        } 
    ]
```
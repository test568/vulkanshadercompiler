#include "utils/utilsRW.h"

void UtilsRW::writeInt(std::ostream &_file, int _value) const
{
    _file.write(reinterpret_cast<char *>(&_value), sizeof(_value));
}

int UtilsRW::readInt(std::istream &_file) const
{
    int value = 0;
    _file.read(reinterpret_cast<char *>(&value), sizeof(value));
    return value;
}

void UtilsRW::writeFloat(std::ostream &_file, float _value) const
{
    _file.write(reinterpret_cast<char *>(&_value), sizeof(_value));
}

float UtilsRW::readFloat(std::istream &_file) const
{
    float value = 0;
    _file.read(reinterpret_cast<char *>(&value), sizeof(value));
    return value;
}

void UtilsRW::writeString(std::ostream &_file, const std::string &_str) const
{
    int size = _str.size();
    writeInt(_file, size);
    _file.write(_str.data(), size);
}

std::string UtilsRW::readString(std::istream &_file) const
{
    int size = readInt(_file);
    if (size <= 0)
    {
        return "";
    }

    char *buffer = new char[size + 1];
    buffer[size] = '\0';
    _file.read(buffer, size);
    return std::string(buffer);
}

void UtilsRW::writeBool(std::ostream &_file, bool &_value) const
{
    _file.write(reinterpret_cast<char *>(&_value), sizeof(_value));
}

bool UtilsRW::readBool(std::istream &_file) const
{
    bool value = 0;
    _file.read(reinterpret_cast<char *>(&value), sizeof(value));
    return value;
}

void UtilsRW::writeBufferUInt(std::ostream &_file, const std::vector<unsigned int> &_buf) const
{
    writeInt(_file, (int)_buf.size());
    _file.write(reinterpret_cast<const char *>(_buf.data()), _buf.size() * sizeof(unsigned int));
}

bool UtilsRW::readBufferUInt(std::istream &_file, std::vector<unsigned int> &_buf) const
{
    int size = readInt(_file);
    if (size < 0)
    {
        return false;
    }
    _buf.clear();
    _buf.resize(size, 0);
    _file.read(reinterpret_cast<char *>(_buf.data()), size * sizeof(unsigned int));
    return true;
}
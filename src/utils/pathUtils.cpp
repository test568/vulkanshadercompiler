#include "utils/pathUtils.h"

std::string PathUtils::getRootDirectoryFile(const std::string &_pathToFile)
{
    int indexRoot = -1;
    for (int i = _pathToFile.size() - 1; i != 0; i--)
    {
        char c = _pathToFile[i];
        if (c == '\\' || c == '/')
        {
            indexRoot = i;
            break;
        }
    }

    if (indexRoot == -1)
    {
        return "";
    }
    else
    {
        return _pathToFile.substr(0, indexRoot + 1);
    }
}

std::string PathUtils::combinePaths(const std::string &_firstPath, const std::string &_secondPath)
{
    std::string result = _firstPath;
    char endFirstPath = _firstPath[_firstPath.size() - 1];
    char beginSecondPath = _secondPath[0];
    if (endFirstPath != '/' && endFirstPath != '\\')
    {
        result += '/';
    }

    if (_secondPath.size() > 2)
    {
        if (beginSecondPath == '.' && (_secondPath[1] == '/' || _secondPath[1] == '\\'))
        {
            result += _secondPath.substr(2);
        }
        else if (beginSecondPath == '/' || beginSecondPath == '\\')
        {
            result += _secondPath.substr(1);
        }
        else
        {
            result += _secondPath;
        }
    }
    else
    {
        if (beginSecondPath == '/' || beginSecondPath == '\\')
        {
            result += _secondPath.substr(1);
        }
        else
        {
            result += _secondPath;
        }
    }

    return result;
}

std::string PathUtils::correctFolderPath(const std::string &_pathToFolder)
{
    std::string result = _pathToFolder;
    char endSymbol = result[result.size() - 1];
    if (endSymbol != '/' && endSymbol != '\\')
    {
        result += '/';
    }
    return result;
}
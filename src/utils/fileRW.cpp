#include <fstream>
#include <sstream>
#include <vector>

#include "utils/file.h"
#include "utils/fileRW.h"

static void getPathData(const std::string &_path, std::string &_name, std::string &_extension, std::string &_root)
{
    int indexPoint = -1;
    int indexRoot = -1;

    for (int i = _path.size() - 1; i != 0; i--)
    {
        char c = _path[i];
        if (c == '.' && indexPoint == -1)
        {
            indexPoint = i;
        }

        if (c == '\\' || c == '/')
        {
            indexRoot = i;
            break;
        }
    }

    if (indexPoint != -1)
    {
        _extension = _path.substr(indexPoint);
    }

    if (indexRoot != -1)
    {
        if (indexPoint != -1)
        {
            _name = _path.substr(indexRoot + 1, _path.size() - indexPoint);
        }
        else
        {
            _name = _path.substr(indexRoot + 1);
        }
        _root = _path.substr(0, indexRoot + 1);
    }
    else
    {
        if (indexPoint != -1)
        {
            _name = _path.substr(0, _path.size() - indexPoint);
        }
        else
        {
            _name = _path;
        }
    }
}

File *FileRW::readFile(const char *_path) const
{
    std::fstream f;
    f.open(_path, std::fstream::in);
    if (!f.is_open())
    {
        return NULL;
    }

    std::string filePath = _path;

    std::string content;

    std::stringstream buffer;
    buffer << f.rdbuf();
    content = buffer.str();
    f.close();

    File *file = new File();
    file->content = content;
    getPathData(_path, file->name, file->extension, file->root);
    return file;
}

bool FileRW::writeFile(const File *_file) const
{
    std::string fullPath = _file->root + _file->name + _file->extension;

    std::fstream f;
    f.open(fullPath, std::fstream::out | std::fstream::trunc);
    if (!f.is_open())
    {
        return false;
    }

    f.write(_file->content.data(), _file->content.size());
    f.flush();
    f.close();
    return true;
}

BinaryFile *FileRW::readBinaryFile(const char *_path) const
{
    std::fstream f;
    f.open(_path, std::fstream::in | std::fstream::binary);
    if (!f.is_open())
    {
        return NULL;
    }

    std::string filePath = _path;

    BinaryFile *file = new BinaryFile();
    file->content = std::vector<char>(std::istreambuf_iterator<char>(f), {});
    f.close();
    getPathData(_path, file->name, file->extension, file->root);
    return file;
}

bool FileRW::writeBinaryFile(const BinaryFile *_file) const
{
    std::string fullPath = _file->root + _file->name + _file->extension;

    std::fstream f;
    f.open(fullPath, std::fstream::out | std::fstream::trunc | std::fstream::binary);
    if (!f.is_open())
    {
        return false;
    }

    f.write(_file->content.data(), _file->content.size());
    f.flush();
    f.close();
    return true;
}
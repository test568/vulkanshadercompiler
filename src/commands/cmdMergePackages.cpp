#include "commands/cmdMergePackages.h"
#include "commands/cmdDefines.h"

#include "utils/pathUtils.h"

#include "packetShadersRW.h"
#include "packetShaders.h"

CmdMergePackages::CmdMergePackages(std::vector<std::string> &_inputFiles, std::string &_outputFolder, std::string &_nameOutputFile)
    : inputFiles(_inputFiles), outputFolder(_outputFolder), nameOutputFile(_nameOutputFile)
{
    PathUtils utils;
    outputFolder = utils.correctFolderPath(outputFolder);
}

CmdMergePackages::~CmdMergePackages()
{
    for (auto i : packetsLoaded)
    {
        delete i;
    }
    packetsLoaded.clear();
}

Command *CmdMergePackages::createCommand(const std::map<std::string, CommandDescription::SettingDatas> &_settings, std::ostream &_errorOutput)
{
    if (_settings.find(INPUT_FILES_SETTING) == _settings.end() || _settings.at(INPUT_FILES_SETTING).stringsDatas.size() == 0)
    {
        _errorOutput << "There is not inputs files." << std::endl;
        return NULL;
    }

    if (_settings.find(OUTPUT_FOLDER_SETTING) == _settings.end() || _settings.at(OUTPUT_FOLDER_SETTING).stringsDatas.size() == 0)
    {
        _errorOutput << "There is not output folder." << std::endl;
        return NULL;
    }

    if ((_settings.find(NAME_OUTPUT_FILE_SETTING) == _settings.end() || _settings.at(NAME_OUTPUT_FILE_SETTING).stringsDatas.size() == 0))
    {
        _errorOutput << "There is not name output file." << std::endl;
        return NULL;
    }

    std::vector<std::string> inputFiles = _settings.at(INPUT_FILES_SETTING).stringsDatas;
    std::string outputFolder = _settings.at(OUTPUT_FOLDER_SETTING).stringsDatas[0];
    std::string nameOutputFile = _settings.at(NAME_OUTPUT_FILE_SETTING).stringsDatas[0];

    return new CmdMergePackages(inputFiles, outputFolder, nameOutputFile);
}

CommandDescription CmdMergePackages::getCommandDescription()
{
    CommandDescription description;
    description.setCommandName("merge");
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Split,
                                   TypeSetting::TypeSetting_Strng, INPUT_FILES_SETTING);
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Unsplit,
                                   TypeSetting::TypeSetting_Strng, OUTPUT_FOLDER_SETTING);
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Unsplit,
                                   TypeSetting::TypeSetting_Strng, NAME_OUTPUT_FILE_SETTING);

    return description;
}

void CmdMergePackages::execute(std::ostream &_errorOutput)
{
    PacketShadersRW packetShadersReaderWriter{};
    for (int i = 0; i < (int)inputFiles.size(); i++)
    {
        std::string path = inputFiles[i];
        PacketShaders *packetShaders = new PacketShaders();
        if (packetShadersReaderWriter.readFromFile(path.c_str(), packetShaders))
        {
            packetsLoaded.push_back(packetShaders);
        }
        else
        {
            delete packetShaders;
            _errorOutput << "Failed to read " << path << std::endl;
            return;
        }
    }

    PacketShaders resultPacketShaders{};
    for (int i = 0; i < (int)packetsLoaded.size(); i++)
    {
        resultPacketShaders.addPacketShaders(packetsLoaded[i]);
    }
    std::string pathSavePacketShaders = std::string(outputFolder + nameOutputFile);
    if (!packetShadersReaderWriter.writeToFile(pathSavePacketShaders.c_str(), &resultPacketShaders))
    {
        _errorOutput << "Failed to write " << pathSavePacketShaders << std::endl;
    }
}
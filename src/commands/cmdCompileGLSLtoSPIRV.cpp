#include <istream>
#include <string>

#include "commands/cmdCompileGLSLtoSPIRV.h"
#include "commands/cmdDefines.h"

#include "shader/shaderConfig.h"
#include "shader/shaderCompiler.h"

#include "shader/packetShaders.h"
#include "shader/packetShadersRW.h"

#include "utils/pathUtils.h"

Command *CmdCompileGLSLtoSPIRV::createCommand(const std::map<std::string, CommandDescription::SettingDatas> &_settings, std::ostream &_errorOutput)
{
    if (_settings.find(INPUT_FILES_SETTING) == _settings.end() || _settings.at(INPUT_FILES_SETTING).stringsDatas.size() == 0)
    {
        _errorOutput << "There is not inputs files." << std::endl;
        return NULL;
    }

    if (_settings.find(OUTPUT_FOLDER_SETTING) == _settings.end() || _settings.at(OUTPUT_FOLDER_SETTING).stringsDatas.size() == 0)
    {
        _errorOutput << "There is not output folder." << std::endl;
        return NULL;
    }

    bool isSeparate = false;
    if (_settings.find(SEPARATE_OUTPUT_FILES_SETTING) != _settings.end())
    {
        isSeparate = _settings.at(SEPARATE_OUTPUT_FILES_SETTING).enable;
    }

    if (!isSeparate && (_settings.find(NAME_OUTPUT_FILE_SETTING) == _settings.end() || _settings.at(NAME_OUTPUT_FILE_SETTING).stringsDatas.size() == 0))
    {
        _errorOutput << "There is not name output file." << std::endl;
        return NULL;
    }

    std::vector<std::string> inputFiles = _settings.at(INPUT_FILES_SETTING).stringsDatas;
    std::string outputFolder = _settings.at(OUTPUT_FOLDER_SETTING).stringsDatas[0];
    std::string nameOutputFile = _settings.at(NAME_OUTPUT_FILE_SETTING).stringsDatas[0];

    return new CmdCompileGLSLtoSPIRV(inputFiles, outputFolder, isSeparate, nameOutputFile);
}

CommandDescription CmdCompileGLSLtoSPIRV::getCommandDescription()
{
    CommandDescription description;
    description.setCommandName("compile");
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Split,
                                   TypeSetting::TypeSetting_Strng, INPUT_FILES_SETTING);
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Unsplit,
                                   TypeSetting::TypeSetting_Strng, OUTPUT_FOLDER_SETTING);
    description.addCommandSettings(1, TypeDataSetting::TypeDataSetting_Unsplit,
                                   TypeSetting::TypeSetting_Strng, NAME_OUTPUT_FILE_SETTING);
    description.addCommandSettings(0, TypeDataSetting::TypeDataSetting_Toogle,
                                   TypeSetting::TypeSetting_Strng, SEPARATE_OUTPUT_FILES_SETTING);
    return description;
}

CmdCompileGLSLtoSPIRV::CmdCompileGLSLtoSPIRV(std::vector<std::string> &_inputFiles, std::string &_outputFolder,
                                             bool &_seprateOutput, std::string &_nameOutputFile)
    : inputFiles(_inputFiles), outputFolder(_outputFolder), seprateOutput(_seprateOutput), nameOutputFile(_nameOutputFile)
{
    PathUtils utils;
    outputFolder = utils.correctFolderPath(outputFolder);
}

CmdCompileGLSLtoSPIRV::~CmdCompileGLSLtoSPIRV()
{
    for (auto i : readShaedrsConfigs)
    {
        delete i;
    }
    readShaedrsConfigs.clear();

    for (auto i : packetsShaders)
    {
        delete i;
    }
    packetsShaders.clear();
    namesPackets.clear();
}

void CmdCompileGLSLtoSPIRV::execute(std::ostream &_errorOutput)
{
    for (auto i : inputFiles)
    {
        ShaderConfig *shaderConfig = ShaderConfig::readShaderConfig(i.data(), _errorOutput);
        if (shaderConfig != NULL)
        {
            readShaedrsConfigs.push_back(shaderConfig);
        }
    }

    std::vector<Shader *> shaders;

    for (auto i : readShaedrsConfigs)
    {
        Shader *shader = compileShader(i, -1, _errorOutput);
        if (shader != NULL)
            shaders.push_back(shader);

        int countVariants = i->getCountVariants();
        for (int j = 0; j < countVariants; j++)
        {
            shader = compileShader(i, j, _errorOutput);
            if (shader != NULL)
                shaders.push_back(shader);
        }
    }

    if (shaders.size() == 0)
    {
        return;
    }

    if (seprateOutput)
    {
        for (int i = 0; i < (int)shaders.size(); i++)
        {
            PacketShaders *packet = new PacketShaders();
            ShaderData data{};
            if (!shaders[i]->getShaderData(&data))
            {
                _errorOutput << "Failed to prepare shader datas from " << std::string(shaders[i]->getName()) << " shader!" << std::endl;
                delete packet;
                return;
            }
            packet->addShader(data);
            namesPackets.push_back(std::string(shaders[i]->getName()));
        }
    }
    else
    {
        PacketShaders *packet = new PacketShaders();
        for (int i = 0; i < (int)shaders.size(); i++)
        {
            ShaderData data{};
            if (!shaders[i]->getShaderData(&data))
            {
                _errorOutput << "Failed to prepare shader datas from " << std::string(shaders[i]->getName()) << " shader!" << std::endl;
                delete packet;
                return;
            }
            writeWarningsInShader(&data, _errorOutput);
            packet->addShader(data);
        }
        packetsShaders.push_back(packet);
        namesPackets.push_back(nameOutputFile);
    }

    for (int i = 0; i < (int)packetsShaders.size(); i++)
    {
        writeShadersPacket(packetsShaders[i], (namesPackets[i]), _errorOutput);
    }
}

Shader *CmdCompileGLSLtoSPIRV::compileShader(ShaderConfig *_shaderConfig, int _indexVariant, std::ostream &_errorOutput)
{
    ShaderCompiler compiler;
    return compiler.compilerShader(_shaderConfig, _indexVariant, _errorOutput);
}

void CmdCompileGLSLtoSPIRV::writeWarningsInShader(ShaderData *_data, std::ostream &_errorOutput)
{
    int countBuffers = _data->uniformBlocks.size();
    for (int i = 0; i < countBuffers; i++)
    {
        UniformBlock *block = &(_data->uniformBlocks[i]);
        if (block->set == -1 && !block->isPushConstant)
        {
            _errorOutput << "Warning: Uniform block " << block->name << " hasn`t set index." << std::endl;
        }
        if (block->binding == -1 && !block->isPushConstant)
        {
            _errorOutput << "Warning: Uniform block " << block->name << " hasn`t binding index." << std::endl;
        }
    }
    int countSamplers = _data->uniformSamplers.size();
    for (int i = 0; i < countSamplers; i++)
    {
        UniformSampler *sampler = &(_data->uniformSamplers[i]);
        if (sampler->set == -1)
        {
            _errorOutput << "Warning: Sampler " << sampler->name << " hasn`t set index." << std::endl;
        }
        if (sampler->binding == -1)
        {
            _errorOutput << "Warning: Sampler " << sampler->name << " hasn`t binding index." << std::endl;
        }
    }
}

void CmdCompileGLSLtoSPIRV::writeShadersPacket(PacketShaders *_packet, const std::string &_name, std::ostream &_errorOutput)
{
    std::string pathSave = outputFolder + _name + ".spak";
    PacketShadersRW writer;
    if (!writer.writeToFile(pathSave.c_str(), _packet))
    {
        _errorOutput << "Failed to write " << pathSave << std::endl;
    }
}
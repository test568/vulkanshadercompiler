#include <fstream>
#include <sstream>

#include "shader/packetShadersRW.h"

void PacketShadersRW::readUniformSamplers(ShaderData *_shaderDatas, std::istream &_in) const
{
    int countUniformSamplers = readInt(_in);
    for (int i = 0; i < countUniformSamplers; i++)
    {
        UniformSampler sampler{};
        sampler.name = readString(_in);
        sampler.set = readInt(_in);
        sampler.binding = readInt(_in);
        sampler.size = readInt(_in);
        sampler.vectorSize = readInt(_in);
        sampler.implicitArraySize = readInt(_in);
        sampler.dataType = (DataType)readInt(_in);
        sampler.samplerFormat = (SamplerFormat)readInt(_in);
        sampler.isImage = readBool(_in);
        sampler.isShadow = readBool(_in);
        sampler.isArray = readBool(_in);
        _shaderDatas->uniformSamplers.push_back(sampler);
    }
}

void PacketShadersRW::writeUniformSamplers(const ShaderData *_shaderDatas, std::ostream &_out) const
{
    int countUniformSamplers = _shaderDatas->uniformSamplers.size();
    writeInt(_out, countUniformSamplers);
    for (int i = 0; i < countUniformSamplers; i++)
    {
        UniformSampler sampler = _shaderDatas->uniformSamplers[i];
        writeString(_out, sampler.name);
        writeInt(_out, sampler.set);
        writeInt(_out, sampler.binding);
        writeInt(_out, sampler.size);
        writeInt(_out, sampler.vectorSize);
        writeInt(_out, sampler.implicitArraySize);
        writeInt(_out, (int)sampler.dataType);
        writeInt(_out, (int)sampler.samplerFormat);
        writeBool(_out, sampler.isImage);
        writeBool(_out, sampler.isShadow);
        writeBool(_out, sampler.isArray);
    }
}

void PacketShadersRW::readUniformBlocks(ShaderData *_shaderDatas, std::istream &_in) const
{
    int countUniformBlocks = readInt(_in);
    for (int i = 0; i < countUniformBlocks; i++)
    {
        UniformBlock block{};
        block.name = readString(_in);
        block.set = readInt(_in);
        block.binding = readInt(_in);
        block.size = readInt(_in);
        block.blockType = (BlockStorageType)readInt(_in);
        block.isPushConstant = readBool(_in);
        int countVariables = readInt(_in);
        for (int i2 = 0; i2 < countVariables; i2++)
        {
            UniformVariable variable{};
            variable.name = readString(_in);
            variable.offset = readInt(_in);
            variable.size = readInt(_in);
            variable.vectorSize = readInt(_in);
            variable.implicitArraySize = readInt(_in);
            variable.type = (DataType)readInt(_in);
            block.variables.push_back(variable);
        }
        _shaderDatas->uniformBlocks.push_back(block);
    }
}

void PacketShadersRW::writeUniformBlocks(const ShaderData *_shaderDatas, std::ostream &_out) const
{
    int countUniformBlocks = _shaderDatas->uniformBlocks.size();
    writeInt(_out, countUniformBlocks);
    for (int i = 0; i < countUniformBlocks; i++)
    {
        UniformBlock block = _shaderDatas->uniformBlocks[i];
        writeString(_out, block.name);
        writeInt(_out, block.set);
        writeInt(_out, block.binding);
        writeInt(_out, block.size);
        writeInt(_out, block.blockType);
        writeBool(_out, block.isPushConstant);
        int countVariables = block.variables.size();
        writeInt(_out, countVariables);
        for (int i2 = 0; i2 < countVariables; i2++)
        {
            UniformVariable variable = block.variables[i2];
            writeString(_out, variable.name);
            writeInt(_out, variable.offset);
            writeInt(_out, variable.size);
            writeInt(_out, variable.vectorSize);
            writeInt(_out, variable.implicitArraySize);
            writeInt(_out, variable.type);
        }
    }
}

void PacketShadersRW::read(PacketShaders *_packetShaders, std::istream &_in) const
{
    int countShaders = readInt(_in);
    for (int i = 0; i < countShaders; i++)
    {
        ShaderData data{};
        data.nameShader = readString(_in);
        data.type = (ShaderType)readInt(_in);
        readBufferUInt(_in, data.spirvCode);
        readUniformSamplers(&data, _in);
        readUniformBlocks(&data, _in);

        _packetShaders->addShader(data);
    }
}

void PacketShadersRW::write(const PacketShaders *_packetShaders, std::ostream &_out) const
{
    int countShaders = _packetShaders->countShaders();
    writeInt(_out, countShaders);
    for (int i = 0; i < countShaders; i++)
    {
        const ShaderData *data = _packetShaders->getShader(i);
        writeString(_out, data->nameShader);
        writeInt(_out, data->type);
        writeBufferUInt(_out, data->spirvCode);
        writeUniformSamplers(data, _out);
        writeUniformBlocks(data, _out);
    }
}

bool PacketShadersRW::readFromFile(const char *_path, PacketShaders *_packetShaders) const
{
    std::fstream f;
    f.open(_path, std::fstream::in | std::fstream::binary);
    if (!f.is_open())
    {
        return false;
    }
    read(_packetShaders, f);
    f.close();
    return true;
}

bool PacketShadersRW::writeToFile(const char *_path, const PacketShaders *_packetShaders) const
{
    std::fstream f;
    f.open(_path, std::fstream::out | std::fstream::trunc | std::fstream::binary);
    if (!f.is_open())
    {
        return false;
    }

    write(_packetShaders, f);
    f.flush();
    f.close();
    return true;
}
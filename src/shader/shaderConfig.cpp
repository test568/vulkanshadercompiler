#include <glslang/Public/ShaderLang.h>
#include <map>
#include <iostream>

#include "shader/shaderConfig.h"

#include "utils/fileRW.h"
#include "utils/pathUtils.h"

#include "plugins/lightconf.hpp"
#include "plugins/config_format.hpp"

#define NAME_SHADER_FIELD "name"
#define VERSION_SHADER_FIELD "version"
#define VERSION_VULKAN_TARGET_FIELD "vulkanVersion"
#define VERSION_SPIRV_TARGET_FIELD "spirvVersion"
#define TYPE_SHADER_FIELD "type"
#define INPUT_FILE_FIELD "file"
#define LIBRARIES_VECTOR_FIELD "libraries"
#define VARIANTS_VECTOR_FIELD "variants"

#define COMPUTE_SHADER_TYPE "compute"
#define VERTEX_SHADER_TYPE "vertex"
#define FRAGMENT_SHADER_TYPE "fragment"

namespace lightconf {
    LIGHTCONF_BEGIN_TYPE(ShaderConfig::Variant)
        LIGHTCONF_TYPE_MEMBER_REQ(std::string, suffix, "suffix")
        LIGHTCONF_TYPE_MEMBER_REQ(std::vector<std::string>, keywords, "keywords")
    LIGHTCONF_END_TYPE()
}

ShaderConfig *ShaderConfig::readShaderConfig(const char *_path, std::ostream &_errorOutput)
{
    FileRW reader = FileRW();
    File *file = reader.readFile(_path);

    if (file == NULL)
    {
        _errorOutput << "Cannot read file: " << std::string(_path) << std::endl;
        return NULL;
    }

    lightconf::group cfgFormat;
    try
    {
        cfgFormat = lightconf::config_format::read(file->content);
    }
    catch (const std::exception &e)
    {
        _errorOutput << e.what() << std::endl;
        return NULL;
    }

    ShaderConfig *shaderConfig = new ShaderConfig();

    if (cfgFormat.has<std::string>(NAME_SHADER_FIELD))
    {
        shaderConfig->setNameShader(cfgFormat.get<std::string>(NAME_SHADER_FIELD).data());
    }
    if (cfgFormat.has<int>(VERSION_SHADER_FIELD))
    {
        shaderConfig->setVersion(cfgFormat.get<int>(VERSION_SHADER_FIELD));
    }
    
    if (cfgFormat.has<double>(VERSION_VULKAN_TARGET_FIELD))
    {
        double versionVulkan = cfgFormat.get<double>(VERSION_VULKAN_TARGET_FIELD);

        switch ((int)(versionVulkan * 10.0))
        {
        case 10:
            shaderConfig->setVulkanTargetVersion(glslang::EShTargetClientVersion::EShTargetVulkan_1_0);
            break;
        case 11:
            shaderConfig->setVulkanTargetVersion(glslang::EShTargetClientVersion::EShTargetVulkan_1_1);
            break;
        case 12:
            shaderConfig->setVulkanTargetVersion(glslang::EShTargetClientVersion::EShTargetVulkan_1_2);
            break;
        case 13:
            shaderConfig->setVulkanTargetVersion(glslang::EShTargetClientVersion::EShTargetVulkan_1_3);
            break;
        default:
            shaderConfig->setVulkanTargetVersion(glslang::EShTargetClientVersion::EShTargetVulkan_1_0);
            break;
        }
    }

    if (cfgFormat.has<double>(VERSION_SPIRV_TARGET_FIELD))
    {
        double versionSPIRV = cfgFormat.get<double>(VERSION_SPIRV_TARGET_FIELD);

        switch ((int)(versionSPIRV * 10.0))
        {
        case 10:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_0);
            break;
        case 11:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_1);
            break;
        case 12:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_2);
            break;
        case 13:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_3);
            break;
        case 14:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_4);
            break;
        case 15:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_5);
            break;
        case 16:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_6);
            break;
        default:
            shaderConfig->setSPIRVTargetVersion(glslang::EShTargetLanguageVersion::EShTargetSpv_1_0);
            break;
        }
    }

    std::map<std::string, EShLanguage> typesShaders;
    typesShaders[COMPUTE_SHADER_TYPE] = EShLanguage::EShLangCompute;
    typesShaders[VERTEX_SHADER_TYPE] = EShLanguage::EShLangVertex;
    typesShaders[FRAGMENT_SHADER_TYPE] = EShLanguage::EShLangFragment;

    if (cfgFormat.has<std::string>(TYPE_SHADER_FIELD))
    {
        std::string shaderType = cfgFormat.get<std::string>(TYPE_SHADER_FIELD);
        if (typesShaders.find(shaderType) != typesShaders.end())
        {
            shaderConfig->setTypeShader(typesShaders[shaderType]);
        }
        else
        {
            _errorOutput << "Shader type: " << shaderType << " not found!" << std::endl;
            shaderConfig->setTypeShader(EShLanguage::EShLangCompute);
        }
    }
    else
    {
        _errorOutput << "Shader type not assigned!" << std::endl;
        shaderConfig->setTypeShader(EShLanguage::EShLangCompute);
    }

    if (cfgFormat.has<std::string>(INPUT_FILE_FIELD))
    {
        shaderConfig->setInputFilePath(cfgFormat.get<std::string>(INPUT_FILE_FIELD).data());
        shaderConfig->setRootPathShader(PathUtils::getRootDirectoryFile(shaderConfig->getInputFilePath()));
    }
    else
    {
        _errorOutput << "Input file not assigned!" << std::endl;
    }

    if (cfgFormat.has<std::vector<std::string>>(LIBRARIES_VECTOR_FIELD))
    {
        std::vector<std::string> libraries = cfgFormat.get<std::vector<std::string>>(LIBRARIES_VECTOR_FIELD);
        for (auto i : libraries)
        {
            shaderConfig->addLibraryFolder(i.data());
        }
    }

    if(cfgFormat.has<std::vector<ShaderConfig::Variant>>(VARIANTS_VECTOR_FIELD))
    {
        std::vector<ShaderConfig::Variant> variants = cfgFormat.get<std::vector<ShaderConfig::Variant>>(VARIANTS_VECTOR_FIELD);
        for (auto i : variants)
            shaderConfig->addVariant(&i);
    }

    return shaderConfig;
}
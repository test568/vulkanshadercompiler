#include "shader/packetShaders.h"

PacketShaders::PacketShaders()
{
}

PacketShaders::~PacketShaders()
{
    for (auto i : shadersDatas)
    {
        delete i;
    }
    shadersDatas.clear();
}

void PacketShaders::addShader(const ShaderData &_shaderData)
{
    ShaderData *shaderData = new ShaderData();
    *shaderData = _shaderData;

    shadersDatas.push_back(shaderData); 
}

void PacketShaders::addPacketShaders(const PacketShaders *_packetShaders)
{
    int count = _packetShaders->countShaders();
    for(int i = 0; i < count; i++)
    {
        addShader(*_packetShaders->getShader(i));
    }
}

int PacketShaders::countShaders() const
{
    return shadersDatas.size();
}

const ShaderData *PacketShaders::getShader(int _index) const
{
    return shadersDatas[_index];
}
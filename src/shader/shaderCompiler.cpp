#include <glslang/Public/ShaderLang.h>
#include <iostream>

#include "shader/shaderCompiler.h"

#include "utils/fileRW.h"
#include "utils/file.h"
#include "utils/pathUtils.h"

ShaderCompiler::IncludeProvider::IncludeProvider(const std::string &_rootShaderFile)
    : rootShaderFile(_rootShaderFile)
{
}

ShaderCompiler::IncludeProvider::~IncludeProvider()
{
    for (auto i : createdIncludes)
    {
        if (i != NULL)
        {
            delete[] i->headerData;
            delete i;
        }
    }
    createdIncludes.clear();
}

glslang::TShader::Includer::IncludeResult *ShaderCompiler::IncludeProvider::includeSystem(const char *_headerName,
                                                                                          const char *_includerName,
                                                                                          size_t _inclusionDepth)
{
    FileRW fileReader;
    File *file = NULL;
    std::string pathToFile;
    int countLibraries = librariesFolders.size();
    for (int i = 0; i < countLibraries; i++)
    {
        std::string pathLibrary = librariesFolders[i];
        pathToFile = PathUtils::combinePaths(pathLibrary, std::string(_headerName));
        file = fileReader.readFile(pathToFile.data());
        if (file)
        {
            break;
        }
    }
    if (!file)
    {
        return NULL;
    }

    int size = file->content.size();
    char *data = new char[size];
    std::memcpy(data, file->content.data(), size);
    delete file;
    IncludeResult *result = new IncludeResult(pathToFile, data, size, NULL);
    createdIncludes.push_back(result);
    return result;
}

glslang::TShader::Includer::IncludeResult *ShaderCompiler::IncludeProvider::includeLocal(const char *_headerName,
                                                                                         const char *_includerName,
                                                                                         size_t _inclusionDepth)
{
    FileRW fileReader;
    File *file = NULL;
    std::string pathToFile;
    if (std::string(_includerName) == "")
    {
        pathToFile = PathUtils::combinePaths(rootShaderFile, std::string(_headerName));
    }
    else
    {
        pathToFile = PathUtils::combinePaths(PathUtils::getRootDirectoryFile(_includerName), std::string(_headerName));
    }
    file = fileReader.readFile(pathToFile.data());
    if (!file)
    {
        return NULL;
    }

    int size = file->content.size();
    char *data = new char[size];
    std::memcpy(data, file->content.data(), size);
    delete file;
    IncludeResult *result = new IncludeResult(pathToFile, data, size, NULL);
    createdIncludes.push_back(result);
    return result;
}

void ShaderCompiler::IncludeProvider::releaseInclude(IncludeResult *_include)
{
    if(_include == NULL)
        return;

    int count = createdIncludes.size();
    for (int i = 0; i < count; i++)
    {
        if (createdIncludes[i] == _include)
        {
            delete[] _include->headerData;
            delete _include;
            createdIncludes[i] = NULL;
            return;
        }
    }
}

Shader *ShaderCompiler::compilerShader(const ShaderConfig *_config, int _indexVariant, std::ostream &_errorOutput)
{
    FileRW fileReader = FileRW();
    File *shaderFile = fileReader.readFile(_config->getInputFilePath());
    if (shaderFile == NULL)
    {
        _errorOutput << "Cannot read file: " << std::string(_config->getInputFilePath()) << std::endl;
        return NULL;
    }

    std::string content = shaderFile->content;
    delete shaderFile;

    glslang::TShader *tshader = new glslang::TShader(_config->getTypeShader());
    std::string version = "#version " + std::to_string(_config->getVersion()) + " core\n";
    std::string extensionGoogleIncules = "#extension GL_GOOGLE_include_directive : require\n";

    std::string shaderName = _config->getNameShader();
    std::vector<std::string> keywords = std::vector<std::string>();
        if(_indexVariant >= 0)
    {
        const ShaderConfig::Variant *variant = _config->getVariant(_indexVariant);

        shaderName += variant->suffix;
        int countKeywords = (int)variant->keywords.size();
        for(int i = 0; i < countKeywords; i++)
            keywords.push_back(std::string("#define " + variant->keywords[i] + "\n"));
    }


    std::vector<const char *> strings;
    strings.push_back(version.data());
    strings.push_back(extensionGoogleIncules.data());
    
    for(int i = 0; i < (int)keywords.size(); i++)
        strings.push_back(keywords[i].data());

    strings.push_back(content.data());

    tshader->setStrings(strings.data(), strings.size());
    tshader->setEnvInput(glslang::EShSource::EShSourceGlsl, _config->getTypeShader(), glslang::EShClient::EShClientVulkan, _config->getVersion());
    tshader->setEnvClient(glslang::EShClient::EShClientVulkan, _config->getVulkanTargetVersion());
    tshader->setEnvTarget(glslang::EShTargetLanguage::EShTargetSpv, _config->getSPIRVTargetVersion());

    TBuiltInResource buildResources = getDefaultResources();
    IncludeProvider includeProvider = IncludeProvider(_config->getRootPathShader());
    int countLibraries = _config->getCountLibrariesFolders();
    for (int i = 0; i < countLibraries; i++)
    {
        includeProvider.addLibraryFolder(_config->getPathToLibraryFolder(i));
    }

    tshader->setAutoMapBindings(true);
    tshader->setAutoMapLocations(true);
    if (!tshader->parse(&buildResources, _config->getVersion(), true, EShMessages::EShMsgDefault, includeProvider))
    {
        _errorOutput << "Failed to compile shader!" << std::endl;
        _errorOutput << std::string(tshader->getInfoLog()) << std::endl;
        delete tshader;
        return NULL;
    }

    glslang::TProgram *tprogram = new glslang::TProgram();
    tprogram->addShader(tshader);
    if (!tprogram->link(EShMsgDefault))
    {
        _errorOutput << "Failed to link program!" << std::endl;
        _errorOutput << std::string(tprogram->getInfoLog()) << std::endl;
    }

    if (!tprogram->buildReflection(EShReflectionAllBlockVariables | EShReflectionIntermediateIO))
    {
        _errorOutput << "Failed to build reflection!" << std::endl;
        delete tshader;
        delete tprogram;
        return NULL;
    }
    tprogram->mapIO();

    return new Shader(shaderName.c_str(), _config->getVersion(), _config->getTypeShader(), tshader, tprogram);
}

TBuiltInResource ShaderCompiler::getDefaultResources()
{
    TBuiltInResource Resources;

    Resources.maxLights = 32;
    Resources.maxClipPlanes = 6;
    Resources.maxTextureUnits = 32;
    Resources.maxTextureCoords = 32;
    Resources.maxVertexAttribs = 64;
    Resources.maxVertexUniformComponents = 4096;
    Resources.maxVaryingFloats = 64;
    Resources.maxVertexTextureImageUnits = 32;
    Resources.maxCombinedTextureImageUnits = 80;
    Resources.maxTextureImageUnits = 32;
    Resources.maxFragmentUniformComponents = 4096;
    Resources.maxDrawBuffers = 32;
    Resources.maxVertexUniformVectors = 128;
    Resources.maxVaryingVectors = 8;
    Resources.maxFragmentUniformVectors = 16;
    Resources.maxVertexOutputVectors = 16;
    Resources.maxFragmentInputVectors = 15;
    Resources.minProgramTexelOffset = -8;
    Resources.maxProgramTexelOffset = 7;
    Resources.maxClipDistances = 8;
    Resources.maxComputeWorkGroupCountX = 65535;
    Resources.maxComputeWorkGroupCountY = 65535;
    Resources.maxComputeWorkGroupCountZ = 65535;
    Resources.maxComputeWorkGroupSizeX = 1024;
    Resources.maxComputeWorkGroupSizeY = 1024;
    Resources.maxComputeWorkGroupSizeZ = 64;
    Resources.maxComputeUniformComponents = 1024;
    Resources.maxComputeTextureImageUnits = 16;
    Resources.maxComputeImageUniforms = 8;
    Resources.maxComputeAtomicCounters = 8;
    Resources.maxComputeAtomicCounterBuffers = 1;
    Resources.maxVaryingComponents = 60;
    Resources.maxVertexOutputComponents = 64;
    Resources.maxGeometryInputComponents = 64;
    Resources.maxGeometryOutputComponents = 128;
    Resources.maxFragmentInputComponents = 128;
    Resources.maxImageUnits = 8;
    Resources.maxCombinedImageUnitsAndFragmentOutputs = 8;
    Resources.maxCombinedShaderOutputResources = 8;
    Resources.maxImageSamples = 0;
    Resources.maxVertexImageUniforms = 0;
    Resources.maxTessControlImageUniforms = 0;
    Resources.maxTessEvaluationImageUniforms = 0;
    Resources.maxGeometryImageUniforms = 0;
    Resources.maxFragmentImageUniforms = 8;
    Resources.maxCombinedImageUniforms = 8;
    Resources.maxGeometryTextureImageUnits = 16;
    Resources.maxGeometryOutputVertices = 256;
    Resources.maxGeometryTotalOutputComponents = 1024;
    Resources.maxGeometryUniformComponents = 1024;
    Resources.maxGeometryVaryingComponents = 64;
    Resources.maxTessControlInputComponents = 128;
    Resources.maxTessControlOutputComponents = 128;
    Resources.maxTessControlTextureImageUnits = 16;
    Resources.maxTessControlUniformComponents = 1024;
    Resources.maxTessControlTotalOutputComponents = 4096;
    Resources.maxTessEvaluationInputComponents = 128;
    Resources.maxTessEvaluationOutputComponents = 128;
    Resources.maxTessEvaluationTextureImageUnits = 16;
    Resources.maxTessEvaluationUniformComponents = 1024;
    Resources.maxTessPatchComponents = 120;
    Resources.maxPatchVertices = 32;
    Resources.maxTessGenLevel = 64;
    Resources.maxViewports = 16;
    Resources.maxVertexAtomicCounters = 0;
    Resources.maxTessControlAtomicCounters = 0;
    Resources.maxTessEvaluationAtomicCounters = 0;
    Resources.maxGeometryAtomicCounters = 0;
    Resources.maxFragmentAtomicCounters = 8;
    Resources.maxCombinedAtomicCounters = 8;
    Resources.maxAtomicCounterBindings = 1;
    Resources.maxVertexAtomicCounterBuffers = 0;
    Resources.maxTessControlAtomicCounterBuffers = 0;
    Resources.maxTessEvaluationAtomicCounterBuffers = 0;
    Resources.maxGeometryAtomicCounterBuffers = 0;
    Resources.maxFragmentAtomicCounterBuffers = 1;
    Resources.maxCombinedAtomicCounterBuffers = 1;
    Resources.maxAtomicCounterBufferSize = 16384;
    Resources.maxTransformFeedbackBuffers = 4;
    Resources.maxTransformFeedbackInterleavedComponents = 64;
    Resources.maxCullDistances = 8;
    Resources.maxCombinedClipAndCullDistances = 8;
    Resources.maxSamples = 4;
    Resources.maxMeshOutputVerticesNV = 256;
    Resources.maxMeshOutputPrimitivesNV = 512;
    Resources.maxMeshWorkGroupSizeX_NV = 32;
    Resources.maxMeshWorkGroupSizeY_NV = 1;
    Resources.maxMeshWorkGroupSizeZ_NV = 1;
    Resources.maxTaskWorkGroupSizeX_NV = 32;
    Resources.maxTaskWorkGroupSizeY_NV = 1;
    Resources.maxTaskWorkGroupSizeZ_NV = 1;
    Resources.maxMeshViewCountNV = 4;

    Resources.limits.nonInductiveForLoops = 1;
    Resources.limits.whileLoops = 1;
    Resources.limits.doWhileLoops = 1;
    Resources.limits.generalUniformIndexing = 1;
    Resources.limits.generalAttributeMatrixVectorIndexing = 1;
    Resources.limits.generalVaryingIndexing = 1;
    Resources.limits.generalSamplerIndexing = 1;
    Resources.limits.generalVariableIndexing = 1;
    Resources.limits.generalConstantMatrixVectorIndexing = 1;

    return Resources;
}
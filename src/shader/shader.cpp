#include <glslang/SPIRV/GlslangToSpv.h>
#include <iostream>
#include <algorithm>
#include "shader/shader.h"

Shader::Shader(const char *_name, int _version, EShLanguage _type, glslang::TShader *_tshader, glslang::TProgram *_tprogram)
    : name(_name), version(_version), type(_type), tshader(_tshader), tprogram(_tprogram)
{
}

Shader::~Shader()
{
    delete tshader;
    delete tprogram;
}

static DataType getTypeData(glslang::TBasicType tBasicType)
{
    switch (tBasicType)
    {
    case glslang::TBasicType::EbtFloat:
        return DataType::DataType_Float;
    case glslang::TBasicType::EbtDouble:
        return DataType::DataType_Double;
    case glslang::TBasicType::EbtFloat16:
        return DataType::DataType_Float16;
    case glslang::TBasicType::EbtInt8:
        return DataType::DataType_Int8;
    case glslang::TBasicType::EbtUint8:
        return DataType::DataType_Uint8;
    case glslang::TBasicType::EbtInt16:
        return DataType::DataType_Int16;
    case glslang::TBasicType::EbtUint16:
        return DataType::DataType_Uint16;
    case glslang::TBasicType::EbtInt:
        return DataType::DataType_Int;
    case glslang::TBasicType::EbtUint:
        return DataType::DataType_Uint;
    case glslang::TBasicType::EbtInt64:
        return DataType::DataType_Int64;
    case glslang::TBasicType::EbtUint64:
        return DataType::DataType_Uint64;
    case glslang::TBasicType::EbtBool:
        return DataType::DataType_Bool;
    default:
        return DataType::DataType_Int;
    }
}

static SamplerFormat getSamplerFormat(glslang::TSamplerDim tSamplerDim)
{
    switch (tSamplerDim)
    {
    case glslang::TSamplerDim::Esd1D:
        return SamplerFormat::SamplerFormat_1D;
    case glslang::TSamplerDim::Esd2D:
        return SamplerFormat::SamplerFormat_2D;
    case glslang::TSamplerDim::Esd3D:
        return SamplerFormat::SamplerFormat_3D;
    case glslang::TSamplerDim::EsdCube:
        return SamplerFormat::SamplerFormat_Cube;
    case glslang::TSamplerDim::EsdRect:
        return SamplerFormat::SamplerFormat_Rect;
    case glslang::TSamplerDim::EsdBuffer:
        return SamplerFormat::SamplerFormat_Buffer;
    default:
        return SamplerFormat::SamplerFormat_2D;
    }
}

static BlockStorageType getBlockStorageFormat(glslang::TBlockStorageClass tblockStorageClass)
{
    switch (tblockStorageClass)
    {
    case glslang::TBlockStorageClass::EbsUniform:
        return BlockStorageType::BlockStorage_UniformBuffer;
    case glslang::TBlockStorageClass::EbsStorageBuffer:
        return BlockStorageType::BlockStorage_StorageBuffer;
    case glslang::TBlockStorageClass::EbsPushConstant:
        return BlockStorageType::BlockStorage_PushConstant;
    default:
        return BlockStorageType::BlockStorage_PushConstant;
    }
}

static ShaderType getShaderType(EShLanguage _type)
{
    switch (_type)
    {
    case EShLanguage::EShLangVertex:
        return ShaderType_Vertex;
    case EShLanguage::EShLangFragment:
        return ShaderType_Fragment;
    case EShLanguage::EShLangCompute:
        return ShaderType_Compute;
    default:
        return ShaderType_Vertex;
    }
}

bool Shader::getShaderData(ShaderData *_data)
{
    _data->nameShader = name;

    _data->type = getShaderType(type);
    _data->spirvCode.clear();

    int countUniformBlocks = tprogram->getNumLiveUniformBlocks();
    for (int i = 0; i < countUniformBlocks; i++)
    {
        glslang::TObjectReflection glslblock = tprogram->getUniformBlock(i);

        UniformBlock block;
        block.name = glslblock.name;
        block.size = glslblock.size;
        block.isPushConstant = glslblock.getType()->getQualifier().isPushConstant();
        block.blockType = getBlockStorageFormat(glslblock.getType()->getQualifier().getBlockStorage());

        if (glslblock.getType()->getQualifier().hasSet())
        {
            block.set = glslblock.getType()->getQualifier().layoutSet;
        }
        else
        {
            block.set = -1;
        }
        block.binding = glslblock.getBinding();
        _data->uniformBlocks.push_back(block);
    }

    int countUniformVariables = tprogram->getNumLiveUniformVariables();
    for (int i = 0; i < countUniformVariables; i++)
    {
        glslang::TObjectReflection uniform = tprogram->getUniform(i);
        if (uniform.getType()->getBasicType() == glslang::TBasicType::EbtSampler)
        {
            UniformSampler uniformSampler;
            uniformSampler.name = uniform.name;
            if (uniform.getType()->getQualifier().hasSet())
            {
                uniformSampler.set = uniform.getType()->getQualifier().layoutSet;
            }
            else
            {
                uniformSampler.set = -1;
            }
            uniformSampler.binding = uniform.getBinding();
            uniformSampler.samplerFormat = getSamplerFormat(uniform.getType()->getSampler().dim);
            uniformSampler.dataType = getTypeData(uniform.getType()->getSampler().getBasicType());
            uniformSampler.size = uniform.size;
            uniformSampler.vectorSize = uniform.getType()->getSampler().vectorSize;
            uniformSampler.isImage = uniform.getType()->getSampler().isImage();
            uniformSampler.isShadow = uniform.getType()->getSampler().isShadow();
            uniformSampler.isArray = uniform.getType()->getSampler().isArrayed();

            if (uniform.getType()->isArray())
            {
                uniformSampler.implicitArraySize = uniform.getType()->getArraySizes()->getCumulativeSize();
            }
            else
            {
                uniformSampler.implicitArraySize = 1;
            }

            _data->uniformSamplers.push_back(uniformSampler);
        }
        else if (uniform.index != -1)
        {
            UniformVariable variable;
            variable.name = uniform.name;
            variable.offset = uniform.offset;
            variable.size = uniform.size;
            variable.vectorSize = uniform.getType()->getVectorSize();
            variable.type = getTypeData(uniform.getType()->getBasicType());

            if (uniform.getType()->isArray() && uniform.getType()->isSizedArray())
            {
                variable.implicitArraySize = uniform.getType()->getArraySizes()->getCumulativeSize();
            }
            else
            {
                variable.implicitArraySize = 1;
            }

            _data->uniformBlocks[uniform.index].variables.push_back(variable);
        }
    }

    std::sort(_data->uniformBlocks.begin(), _data->uniformBlocks.end(), [](const UniformBlock &_a, const UniformBlock &_b)
              { return _a.binding < _b.binding; });
    std::sort(_data->uniformSamplers.begin(), _data->uniformSamplers.end(), [](const UniformSampler &_a, const UniformSampler &_b)
              { return _a.binding < _b.binding; });

    glslang::GlslangToSpv(*tprogram->getIntermediate(type), _data->spirvCode);
    return true;
}
#include <cstring>

#include "commandReader/commandReader.h"

void CommandReader::addCommandType(CommandDescription _description, CreateCommand _createFunctions)
{
    commandDescriptions.push_back(std::make_pair(_description, _createFunctions));
}

bool CommandReader::readCommands(int _argc, char *_argv[], std::vector<Command *> *_outBufferCommands, std::ostream &_errorOutput)
{
    if (_argc < 1)
    {
        return true;
    }

    std::string argCommandName = std::string(_argv[0]);
    CommandDescription *descriptionCommand = NULL;
    std::map<std::string, CommandDescription::SettingDatas> settingDatas;

    int indexDescription = -1;
    for (int i = 0; i < (int)commandDescriptions.size(); i++)
    {
        if (std::strcmp(argCommandName.c_str(), commandDescriptions[i].first.getCommandName()) == 0)
        {
            descriptionCommand = &(commandDescriptions[i].first);
            indexDescription = i;
            break;
        }
    }

    if (indexDescription == -1)
    {
        _errorOutput << "Command: " << argCommandName << " not found!" << std::endl;
        return false;
    }

    const CommandDescription::CommandSetting *currentSetting = NULL;

    for (int i = 1; i < _argc; i++)
    {
        std::string argString = std::string(_argv[i]);
        if (argString[0] == '-')
        {
            std::string subSetting = argString.substr(1);

            const CommandDescription::CommandSetting *newSetting = NULL;
            for (int i2 = 0; i2 < descriptionCommand->getCountSettings(); i2++)
            {
                const CommandDescription::CommandSetting *testSetting = descriptionCommand->getCommandSetting(i2);
                std::string settingName = testSetting->nameSetting;
                if (testSetting->typeGettingSetting == TypeDataSetting::TypeDataSetting_Split ||
                    testSetting->typeGettingSetting == TypeDataSetting::TypeDataSetting_Toogle)
                {
                    if (std::strcmp(settingName.c_str(), subSetting.c_str()) == 0)
                    {
                        newSetting = testSetting;
                        break;
                    }
                }
                else
                {
                    if (std::strcmp(settingName.c_str(), subSetting.substr(0, settingName.size()).c_str()) == 0)
                    {
                        newSetting = testSetting;
                        break;
                    }
                }
            }

            if (newSetting == NULL)
            {
                _errorOutput << "Setting: " << subSetting << " not found!" << std::endl;
                return false;
            }

            currentSetting = newSetting;
            setEnableSetting(currentSetting, settingDatas);

            if (currentSetting->typeGettingSetting == TypeDataSetting::TypeDataSetting_Unsplit)
            {
                int startPosData = currentSetting->nameSetting.size();
                std::string data = subSetting.substr(startPosData);
                if (data.size() == 0)
                {
                    _errorOutput << "Setting: " << subSetting << " hasn`t data!" << std::endl;
                    return false;
                }

                addSettingData(currentSetting, data, settingDatas, _errorOutput);
            }
            continue;
        }

        if (currentSetting == NULL || currentSetting->typeGettingSetting == TypeDataSetting::TypeDataSetting_Unsplit)
        {
            _errorOutput << "Setting: " << argString << " not found!" << std::endl;
            return false;
        }

        if (currentSetting->typeGettingSetting != TypeDataSetting::TypeDataSetting_Toogle)
        {
            addSettingData(currentSetting, argString, settingDatas, _errorOutput);
        }
    }

    for (auto i : settingDatas)
    {
        std::string nameSetting = i.first;
        for (int i2 = 0; i2 < descriptionCommand->getCountSettings(); i2++)
        {
            std::string checkSettingName = descriptionCommand->getCommandSetting(i2)->nameSetting;
            int countDatasMinimum = descriptionCommand->getCommandSetting(i2)->countDatas;
            if (std::strcmp(nameSetting.c_str(), checkSettingName.c_str()) == 0)
            {
                if ((int)i.second.integersDatas.size() < countDatasMinimum &&
                    (int)i.second.numbersDatas.size() < countDatasMinimum &&
                    (int)i.second.stringsDatas.size() < countDatasMinimum)
                {
                    _errorOutput << "Setting: " << nameSetting << " expected minimum " << countDatasMinimum << " params!" << std::endl;
                    return false;
                }
                break;
            }
        }
    }

    _outBufferCommands->push_back(commandDescriptions[indexDescription].second(settingDatas, _errorOutput));
    return true;
}

void CommandReader::addSettingData(const CommandDescription::CommandSetting *_setting, const std::string &_data, std::map<std::string, CommandDescription::SettingDatas> &_outSetSettings, std::ostream &_errorOutput)
{
    CommandDescription::SettingDatas *settingDatas;
    if (_outSetSettings.find(_setting->nameSetting) == _outSetSettings.end())
    {
        _outSetSettings[_setting->nameSetting] = CommandDescription::SettingDatas{};
    }
    settingDatas = &(_outSetSettings[_setting->nameSetting]);

    if (_setting->typeSetting == TypeSetting::TypeSetting_Strng)
    {
        settingDatas->stringsDatas.push_back(_data);
    }
    else if (_setting->typeSetting == TypeSetting::TypeSetting_Number)
    {
        try
        {
            settingDatas->numbersDatas.push_back(std::stof(_data));
        }
        catch (const std::exception &e)
        {
            _errorOutput << _data << " is not number!" << std::endl;
        }
    }
    else if (_setting->typeSetting == TypeSetting::TypeSetting_Integer)
    {
        try
        {
            settingDatas->numbersDatas.push_back(std::stoi(_data));
        }
        catch (const std::exception &e)
        {
            _errorOutput << _data << " is not integer!" << std::endl;
        }
    }
}

void CommandReader::setEnableSetting(const CommandDescription::CommandSetting *_setting, std::map<std::string, CommandDescription::SettingDatas> &_outSetSettings)
{
    CommandDescription::SettingDatas *settingDatas;
    if (_outSetSettings.find(_setting->nameSetting) == _outSetSettings.end())
    {
        _outSetSettings[_setting->nameSetting] = CommandDescription::SettingDatas{};
    }
    settingDatas = &_outSetSettings[_setting->nameSetting];
    settingDatas->enable = true;
}
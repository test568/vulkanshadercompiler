#include "commandReader/commandDescription.h"

void CommandDescription::addCommandSettings(int _countDatas, TypeDataSetting _typeData, TypeSetting _type, const char *_name)
{
    CommandSetting setting{};
    setting.countDatas = _countDatas;
    setting.nameSetting = std::string(_name);
    setting.typeSetting = _type;
    setting.typeGettingSetting = _typeData;
    settings.push_back(setting);
}
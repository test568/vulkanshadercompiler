#include "commandExecutor/commandExecutor.h"

void CommandExecutor::executeCommand(Command *_command, std::ostream &_errorOutputs)
{
    _command->execute(_errorOutputs);
}
#include <iostream>
#include <map>
#include <string>

#include <glslang/Public/ShaderLang.h>

#include "commandReader/command.h"
#include "commandReader/commandReader.h"

#include "commandExecutor/commandExecutor.h"
#include "commands/cmdCompileGLSLtoSPIRV.h"
#include "commands/cmdMergePackages.h"

int main(int argc, char *argv[])
{
    glslang::InitializeProcess();

    CommandReader commandReader;
    commandReader.addCommandType(CmdCompileGLSLtoSPIRV::getCommandDescription(), &CmdCompileGLSLtoSPIRV::createCommand);
    commandReader.addCommandType(CmdMergePackages::getCommandDescription(), &CmdMergePackages::createCommand);

    std::vector<Command *> commandsBuffer;
    commandReader.readCommands(argc - 1, &(argv[1]), &commandsBuffer, std::cerr);

    CommandExecutor commandExecutor;
    for (int i = 0; i < (int)commandsBuffer.size(); i++)
    {
        commandExecutor.executeCommand(commandsBuffer[i], std::cerr);
    }

    glslang::FinalizeProcess();
}
